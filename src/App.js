import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import Rep from './Rep';

class App extends Component {
  constructor () {
    super ();
    this.state = {
      postalCodeInput: '',
      mpRep: [],
      representative: '',
    };
    this.handleChange = this.handleChange.bind (this);
  }
  //Update state when the text input changes
  handleChange (e) {
    this.setState ({
      [e.target.name]: e.target.value,
    });
  }

  fetchData (code) {
    axios
      .get (`https://represent.opennorth.ca/postcodes/${code}`)
      .then (res => {
        this.setState ({
          mpRep: res.data.representatives_centroid,
        });
      });
  }

  hoverButton = () => {
    this.setState ({
      hoverButton: true,
    });
  };

  notHoverButton = () => {
    this.setState ({
      hoverButton: false,
    });
  };

  render () {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">
          {/* Enter your postal code to find your MP! */}
        </p>
        <br />
        <input
          onChange={this.handleChange}
          style={{
            width: 350,
            paddingLeft: 7.5,
            borderRadius: 10,
            border: '1px lightgrey solid',
            marginBottom: 10,
            fontSize: 18,
          }}
          name="postalCodeInput"
          placeholder="Enter your postal code to find your MP!"
        />
        <center>
          <select
            onChange={this.handleChange}
            name="representative"
            style={{
              display: 'block',
              width: 350,
              fontSize: 18,
              marginBottom: 10,
            }}
          >
            <option>Select Which Representative You Wish To View</option>
            <option value="MP">MP</option>
            <option value="Mayor">Mayor</option>
            <option value="Councillor">Councillor</option>
            <option value="MLA">MLA</option>
          </select>
        </center>
        <button
          onClick={this.fetchData (this.state.postalCodeInput)}
          onMouseOver={this.hoverButton}
          onMouseOut={this.notHoverButton}
          style={{
            width: 350,
            fontSize: 18,
            cursor: 'pointer',
            backgroundColor: this.state.hoverButton
              ? 'rgba(25,225,100,0.6)'
              : 'white',
            borderRadius: 10,
          }}
        >
          Go
        </button>
        <div>
          {this.state.mpRep.map (el => (
            <Rep representative={this.state.representative} el={el} />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
