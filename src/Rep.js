import React, {Component} from 'react';

class Rep extends Component {
  constructor (props) {
    super (props);
  }
  render () {
    return (
      <h3
        style={{
          display: this.props.el.elected_office == this.props.representative
            ? 'inline-block'
            : 'none',
        }}
      >
        <center>
          <div
            style={{
              width: 100,
              height: 100,
              overflow: 'hidden',
              borderRadius: 50,
            }}
          >
            <img
              style={{
                overflow: 'hidden',
                display: 'block',
                width: '100%',
                height: 'auto',
                marginBottom: 10,
              }}
              src={this.props.el.photo_url}
            />
          </div>
        </center>
        {this.props.el.elected_office}:
        {' '}{this.props.el.name}<br />
        {/* Didn't see the first_name key but this demonstrates some problem solving capacity :) */}
        {/* Email {el.name.split (' ')[0]}: {el.email} */}
        {this.props.el.email == ''
          ? "This Rep Doesn't have a publicly available email address"
          : `Email ${this.props.el.first_name}: ${this.props.el.email}`}
      </h3>
    );
  }
}

export default Rep;
